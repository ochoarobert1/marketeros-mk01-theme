<?php get_header('mantenimiento'); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-mantenimiento col-lg-12 col-md-12 col-sm-12 col-xs-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <article>
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="<?php echo get_bloginfo('name'); ?>" class="img-responsive" />
                <h1><?php _e('Sitio en construcción', 'marketeros'); ?></h1>
            </article>
        </section>
    </div>
</main>
<?php get_footer('mantenimiento'); ?>
