<?php
/**
 * Template Name: Plantilla para Pagina de Inicio
 *
 * @package Marketeros Rockstar
 * @subpackage marketeros-mk01-theme
 * @since 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php /* MAIN SLIDER - BUILT BY REVOLUTION SLIDER */?>
        <?php $array_posts = array(); ?>
        <h1 class="hidden-h1"><?php echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); ?></h1>
        <section class="the-slider col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div id="main-carousel" class=" owl-carousel owl-theme">
                            <?php $args = array('post_type' => 'post', 'posts_per_page' => 4, 'order' => 'DESC', 'orderby' => 'date');?>
                            <?php query_posts($args); ?>
                            <?php if (have_posts()) : ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <?php array_push($array_posts, get_the_ID());  ?>
                            <?php if (has_post_thumbnail()) { ?>
                            <div class="main-slider-item col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <?php the_post_thumbnail('slider_img', array('class' => 'img-responsive')); ?>

                                <div class="main-slider-item-wrapper">
                                    <?php $categories = get_the_category(); ?>
                                    <div class="main-slider-categories">
                                        <?php foreach ($categories as $item) { ?>
                                        <a href="<?php echo get_category_link($item->term_id); ?>" title="<?php echo $item->name; ?>"><?php echo $item->name; ?></a>
                                        <?php } ?>
                                    </div>
                                    <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                        <?php $subtitulo = get_post_meta(get_the_ID(), 'rw_post_subtitle', true); ?>
                                        <?php if ($subtitulo != '') { ?>
                                        <h3><?php echo $subtitulo; ?></h3>
                                        <?php } ?>
                                        <h2><?php the_title(); ?></h2>
                                    </a>
                                </div>


                            </div>
                            <?php } ?>
                            <?php endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="home-main-section col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="home-main-section-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="home-main-posts-container col-lg-8 col-md-8 col-sm-8 col-xs-12 no-paddingl">
                            <?php include(locate_template('templates/templates-home-blocks.php')); ?>
                        </div>
                        <aside class="home-sidebar home-sidebar-1 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <?php if ( is_active_sidebar( 'home_sidebar' ) ) : ?>
                            <ul id="sidebar">
                                <?php dynamic_sidebar( 'home_sidebar' ); ?>
                            </ul>
                            <?php endif; ?>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <section class="home-main-special-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="home-main-special-content col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <?php $args = array('post_type' => 'post', 'posts_per_page' => 1, 'order' => 'DESC', 'orderby' => 'date', 'ignore_sticky_posts' => 1, 'post__not_in' => $array_posts ); ?>
                        <?php query_posts($args); ?>
                        <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <?php array_push($array_posts, get_the_ID());  ?>
                        <div class="home-main-special-item-big col-md-6 no-paddingl">
                            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                            </a>

                            <div class="home-main-special-item-big-wrapper">
                                <?php $categories = get_the_category(); ?>
                                <div class="main-item-categories">
                                    <?php foreach ($categories as $item) { ?>
                                    <a href="<?php echo get_category_link($item->term_id); ?>" title="<?php echo $item->name; ?>"><?php echo $item->name; ?></a>
                                    <?php } ?>
                                </div>
                                <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                    <?php $subtitulo = get_post_meta(get_the_ID(), 'rw_post_subtitle', true); ?>
                                    <?php if ($subtitulo != '') { ?>
                                    <h4><?php echo $subtitulo; ?></h4>
                                    <?php } ?>
                                    <h3><?php the_title(); ?></h3>
                                </a>

                            </div>

                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                        <div class="home-main-special-item-small-container col-md-6">
                            <?php $args = array('post_type' => 'post', 'posts_per_page' => 4, 'order' => 'DESC', 'orderby' => 'date', 'ignore_sticky_posts' => 1, 'post__not_in' => $array_posts ); ?>
                            <?php query_posts($args); ?>
                            <?php if (have_posts()) : ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <?php array_push($array_posts, get_the_ID());  ?>
                            <article class="home-main-special-item-small col-md-6">
                                <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                    <?php if (has_post_thumbnail()) { ?>
                                    <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                                    <?php } else { ?>
                                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/no-img-squared.jpg" alt="<?php echo get_the_title(); ?>" class="img-responsive" />
                                    <?php } ?>
                                </a>
                                <div class="home-main-special-item-small-wrapper">
                                    <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                        <?php $subtitulo = get_post_meta(get_the_ID(), 'rw_post_subtitle', true); ?>
                                        <?php if ($subtitulo != '') { ?>
                                        <h4><?php echo $subtitulo; ?></h4>
                                        <?php } ?>
                                        <h3><?php the_title(); ?></h3>
                                    </a>
                                </div>
                            </article>
                            <?php endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <section class="home-main-section col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="home-main-section-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="home-main-posts-container col-lg-8 col-md-8 col-sm-8 col-xs-12 no-padding">
                            <?php include(locate_template('templates/templates-home-blocks.php')); ?>
                        </div>
                        <aside class="home-sidebar home-sidebar-1 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <?php if ( is_active_sidebar( 'home_sidebar-2' ) ) : ?>
                            <ul id="sidebar">
                                <?php dynamic_sidebar( 'home_sidebar-2' ); ?>
                            </ul>
                            <?php endif; ?>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <section class="home-main-special-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="home-main-special-content col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <?php $args = array('post_type' => 'post', 'posts_per_page' => 1, 'order' => 'DESC', 'orderby' => 'date', 'ignore_sticky_posts' => 1, 'post__not_in' => $array_posts, 'category_name' => 'musica' ); ?>
                        <?php query_posts($args); ?>
                        <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <?php array_push($array_posts, get_the_ID());  ?>
                        <div class="home-main-special-item-big col-md-6 no-paddingl">
                            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                            </a>

                            <div class="home-main-special-item-big-wrapper">
                                <?php $categories = get_the_category(); ?>
                                <div class="main-item-categories">
                                    <?php foreach ($categories as $item) { ?>
                                    <a href="<?php echo get_category_link($item->term_id); ?>" title="<?php echo $item->name; ?>"><?php echo $item->name; ?></a>
                                    <?php } ?>
                                </div>
                                <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                    <?php $subtitulo = get_post_meta(get_the_ID(), 'rw_post_subtitle', true); ?>
                                    <?php if ($subtitulo != '') { ?>
                                    <h4><?php echo $subtitulo; ?></h4>
                                    <?php } ?>
                                    <h3><?php the_title(); ?></h3>
                                </a>

                            </div>

                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                        <div class="home-main-special-item-small-container col-md-6">
                            <?php $args = array('post_type' => 'post', 'posts_per_page' => 4, 'order' => 'DESC', 'orderby' => 'date', 'ignore_sticky_posts' => 1, 'post__not_in' => $array_posts, 'category_name' => 'musica' ); ?>
                            <?php query_posts($args); ?>
                            <?php if (have_posts()) : ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <?php array_push($array_posts, get_the_ID());  ?>
                            <article class="home-main-special-item-small col-md-6">
                                <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                    <?php if (has_post_thumbnail()) { ?>
                                    <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                                    <?php } else { ?>
                                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/no-img-squared.jpg" alt="<?php echo get_the_title(); ?>" class="img-responsive" />
                                    <?php } ?>
                                </a>
                                <div class="home-main-special-item-small-wrapper">
                                    <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                        <?php $subtitulo = get_post_meta(get_the_ID(), 'rw_post_subtitle', true); ?>
                                        <?php if ($subtitulo != '') { ?>
                                        <h4><?php echo $subtitulo; ?></h4>
                                        <?php } ?>
                                        <h3><?php the_title(); ?></h3>
                                    </a>
                                </div>
                            </article>
                            <?php endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <section class="home-main-special-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="home-main-special-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php $args = array('post_type' => 'videos', 'posts_per_page' => 1, 'order' => 'DESC', 'orderby' => 'date', 'ignore_sticky_posts' => 1  ); ?>
                        <?php query_posts($args); ?>
                        <?php if (have_posts()) : ?>
                        <h2 class="section-video-title"><?php _e('Videos Destacados', 'marketeros'); ?></h2>
                        <?php endif; ?>
                        <div class="home-main-videos-content col-lg-8 col-md-8 col-sm-8 col-xs-12 no-paddingl no-paddingr">
                            <?php $array_videos = array(); ?>
                            <?php $args = array('post_type' => 'videos', 'posts_per_page' => 1, 'order' => 'DESC', 'orderby' => 'date', 'ignore_sticky_posts' => 1  ); ?>
                            <?php query_posts($args); ?>
                            <?php if (have_posts()) : ?>
                            <?php $array_ids = array(); ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <?php array_push($array_videos, get_the_ID());  ?>
                            <?php $url = sum_video_parser(get_post_meta(get_the_ID(), 'rw_post_video', false), false); ?>
                            <div class="embed-responsive embed-responsive-16by9">
                                <?php echo $url['embed']; ?>
                            </div>
                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                        <div class="home-main-videos-playlist col-lg-4 col-md-4 col-sm-4 col-xs-12 no-paddingl no-paddingr">
                            <?php $args = array('post_type' => 'videos', 'posts_per_page' => -1, 'order' => 'DESC', 'orderby' => 'date', 'ignore_sticky_posts' => 1, 'post__not_in' => $array_videos  ); ?>
                            <?php query_posts($args); ?>
                            <?php if (have_posts()) : ?>

                            <?php $array_ids = array(); $i=1; ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <div id="<?php echo get_the_ID(); ?>" onclick="change_video(<?php echo get_the_ID(); ?>)" class="home-main-videos-item">
                                <?php $url = sum_video_parser(get_post_meta(get_the_ID(), 'rw_post_video', false), false); ?>
                                <div class="media-img">
                                    <img src="<?php echo $url['thumb_image']; ?>" alt="<?php the_title(); ?>" class="img-responsive" />
                                </div>
                                <h2><?php the_title(); ?></h2>
                            </div>

                            <?php $i++; endwhile; ?>

                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</main>
<?php get_footer(); ?>
