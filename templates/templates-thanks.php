<?php
/**
 * Template Name: Plantilla para Agradecimiento
 *
 * @package Marketeros Rockstar
 * @subpackage marketeros-mk01-theme
 * @since 1.0
 */
?>
<?php get_header('mantenimiento'); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <div class="thanks-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="<?php _e('Gracias por suscribirte', 'marketeros'); ?>" />
            <?php the_content(); ?>
        </div>
    </div>
</main>
<?php get_footer('mantenimiento'); ?>
