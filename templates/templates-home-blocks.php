<?php $args = array('post_type' => 'post', 'posts_per_page' => 4, 'order' => 'DESC', 'orderby' => 'date', 'ignore_sticky_posts' => 1, 'post__not_in' => $array_posts ); ?>
<?php query_posts($args); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php array_push($array_posts, get_the_ID());  ?>
<article class="home-main-section-item col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl">
    <div class="media-custom col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
       <div class="media-custom-info media-custom-info-mobile hidden-lg hidden-md hidden-sm col-xs-12 no-paddingl no-paddingr">
            <div class="media-custom-info-wrapper">
                <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                    <?php $subtitulo = get_post_meta(get_the_ID(), 'rw_post_subtitle', true); ?>
                    <?php if ($subtitulo != '') { ?>
                    <h3><?php echo $subtitulo; ?></h3>
                    <?php } ?>
                    <h2><?php the_title(); ?></h2>
                </a>
                <div class="date">
                    <span><?php echo get_the_date('d/m/Y'); ?></span>
                </div>
                <?php $categories = get_the_category(); ?>
                <div class="main-item-categories">
                    <?php foreach ($categories as $item) { ?>
                    <a href="<?php echo get_category_link($item->term_id); ?>" title="<?php echo $item->name; ?>"><?php echo $item->name; ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="media-custom-img col-lg-6 col-md-6 col-sm-6 col-xs-12 no-paddingl">
            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                <?php if (has_post_thumbnail()) { ?>
                <?php the_post_thumbnail('blog_img', array('class' => 'img-responsive')); ?>
                <?php } else { ?>
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/no-img.jpg" alt="<?php echo get_the_title(); ?>" class="img-responsive" />
                <?php } ?>
            </a>
        </div>
        <div class="media-custom-info col-lg-6 col-md-6 col-sm-6 hidden-xs">
            <div class="media-custom-info-wrapper">
                <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                    <?php $subtitulo = get_post_meta(get_the_ID(), 'rw_post_subtitle', true); ?>
                    <?php if ($subtitulo != '') { ?>
                    <h3><?php echo $subtitulo; ?></h3>
                    <?php } ?>
                    <h2><?php the_title(); ?></h2>
                </a>
                <div class="date">
                    <span><?php echo get_the_date('d/m/Y'); ?></span>
                </div>
                <?php $categories = get_the_category(); ?>
                <div class="main-item-categories">
                    <?php foreach ($categories as $item) { ?>
                    <a href="<?php echo get_category_link($item->term_id); ?>" title="<?php echo $item->name; ?>"><?php echo $item->name; ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</article>
<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_query(); ?>

