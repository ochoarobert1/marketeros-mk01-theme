<section class="home-main-special-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
    <div class="container">
        <div class="row">
            <div class="home-main-special-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="video-title-section col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2><?php _e('Videos Destacados', 'marketeros'); ?></h2>
                </div>
                <?php $args = array('post_type' => 'post', 'posts_per_page' => 1, 'order' => 'DESC', 'orderby' => 'date', 'ignore_sticky_posts' => 1, 'post__not_in' => $array_posts, 'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug', 'terms' => array('post-format-video'), 'operator' => 'IN' ))); ?>
                <?php query_posts($args); ?>
                <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                <div class="home-main-special-item-big col-lg-8 col-md-8 col-sm-8 col-xs-12 no-paddingl">
                    <div id="home-main-video-player" class="home-main-special-item-big-wrapper">
                        <?php $video = get_post_meta(get_the_ID(), 'rw_post_video', true); ?>
                        <?php $embed = sum_video_parser($video, 0); ?>
                        <div class="embed-responsive embed-responsive-16by9">
                            <?php echo $embed['embed']; ?>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
                <div class="home-main-video-item-small-container col-lg-4 col-md-4 col-sm-4 col-xs-12 no-paddingl">
                    <?php $args = array('post_type' => 'post', 'posts_per_page' => 4, 'order' => 'DESC', 'orderby' => 'date', 'ignore_sticky_posts' => 1, 'post__not_in' => $array_posts, 'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug', 'terms' => array('post-format-video'), 'operator' => 'IN' ))); ?>
                    <?php query_posts($args); ?>
                    <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <?php array_push($array_posts, get_the_ID());  ?>
                    <article class="home-main-video-item-small col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="home-main-video-item-small-wrapper">
                            <div class="media media-video">
                                <?php $video = get_post_meta(get_the_ID(), 'rw_post_video', true); ?>
                                <?php $embed = sum_video_parser($video, 0); ?>
                                <div class="media-left media-middle">
                                    <a onclick="change_video(<?php echo get_the_ID(); ?>);">
                                        <img class="media-object" src="<?php echo $embed['thumb_image']; ?>" alt="<?php echo get_the_title(); ?>">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <a onclick="change_video(<?php echo get_the_ID(); ?>);">
                                        <?php $subtitulo = get_post_meta(get_the_ID(), 'rw_post_subtitle', true); ?>
                                        <?php if ($subtitulo != '') { ?>
                                        <?php echo $subtitulo; ?>
                                        <?php } ?>
                                        <h4 class="media-heading"><?php the_title(); ?></h4>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </article>
                    <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>

            </div>
        </div>
    </div>
</section>
