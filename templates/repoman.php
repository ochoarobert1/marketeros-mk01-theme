/* --------------------------------------------------------------
ADD CUSTOM WIDGETS
-------------------------------------------------------------- */

function wpb_load_widget() {
register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );


class wpb_widget extends WP_Widget {

function __construct() {
parent::__construct(

// Base ID of your widget
'wpb_widget',

// Widget name will appear in UI
__('Entradas Recientes - Marketeros', 'marketeros'),

// Widget description
array( 'description' => __( 'Listado de entradas recientes', 'marketeros' ), )
);
}

// Creating widget front-end

public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
var_dump($instance);
if (!$instance['quantity']) {
$quantity = 5;
} else {
$quantity = $instance['quantity'];
}
if (!$instance['category']) {
$categories = get_category_by_slug('noticias');
$category = $categories->term_id;
} else {
$category = $instance['category'];
}
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
$args2 = array('post_type' => 'post', 'posts_per_page' => $quantity, 'order' => 'DESC', 'orderby' => 'date', 'cat' => $category, 'ignore_sticky_posts' => 1, 'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug', 'terms' => array('post-format-video'), 'operator' => 'NOT IN' )));
query_posts($args2);
if (have_posts()) :
while (have_posts()) : the_post();
?>
<div class="media custom-media">
    <div class="media-left media-middle">
        <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
            <?php if (has_post_thumbnail()) { ?>
            <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'avatar'); ?>
            <img class="media-object" src="<?php echo $featured_img_url; ?>" alt="<?php echo get_the_title(); ?>" />
            <?php } else { ?>
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/no-img-squared.jpg" alt="<?php echo get_the_title(); ?>" class="media-object" />
            <?php } ?>
        </a>
    </div>
    <div class="media-body">
        <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
            <?php $subtitulo = get_post_meta(get_the_ID(), 'rw_post_subtitle', true); ?>
            <?php if ($subtitulo != '') { ?>
            <h3><?php echo $subtitulo; ?></h3>
            <?php } ?>
            <h4 class="media-heading"><?php the_title(); ?></h4>
        </a>
    </div>
</div>
<?php endwhile; ?>
<div class="clearfix"></div>
<?php endif;
wp_reset_query();
echo $args['after_widget'];
}

// Widget Backend
public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) && isset( $instance[ 'quantity' ] ) && isset( $instance[ 'category' ] ) ) {
        $title = $instance[ 'title' ];
        $quantity = $instance[ 'quantity' ];
        $category = $instance[ 'category' ];
    }
    else {
        $title = __( 'Top Marketeros', 'marketeros' );
        $quantity = 5;
        $categories = get_category_by_slug('noticias');
        $category = $categories->term_id;
    }
    // Widget admin form
?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'quantity' ); ?>"><?php _e( 'Cantidad de Entradas:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'quantity' ); ?>" name="<?php echo $this->get_field_name( 'quantity' ); ?>" type="number" value="<?php echo esc_attr( $quantity ); ?>" />
</p>

<p>
    <label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Categoría a colocar:' ); ?></label>
    <select id="<?php echo $this->get_field_id( 'category' ); ?>" name="<?php echo $this->get_field_name( 'category' ); ?>">
        <?php $terms = get_terms( 'category',  array('hide_empty' => 'false')); ?>
        <?php foreach ($terms as $term) { ?>
        <?php if ($term->term_id == $category ) { $class = 'selected="selected"'; } else { $class = ''; } ?>
        <option <?php echo $class; ?> value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
        <?php } ?>
    </select>
</p>

<?php
}

// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance[ 'quantity' ]  = ( ! empty( $new_instance['quantity'] ) ) ? strip_tags( $new_instance['quantity'] ) : '';
    $instance[ 'category' ]  = ( ! empty( $new_instance['category'] ) ) ? strip_tags( $new_instance['category'] ) : '';
    return $instance;
}
} // Class wpb_widget ends here

/* END - CUSTOM WIDGET */
