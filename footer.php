<!-- PIXEL -->
<img src="https://tracker.metricool.com/c3po.jpg?hash=244ba318113a893f4c2e262f6cece633"/>
<!-- PIXEL -->
<div class="gotop">
    <a href="#top" data-scroll title="<?php _e('Volver Arriba'); ?>">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/go_top.png" alt="<?php _e('Volver Arriba'); ?>" class="go-top-btn" />
    </a>
</div>
<footer class="container-fluid" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row">
        <div class="the-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="footer-menu col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <?php if ( is_active_sidebar( 'footer_sidebar' ) ) : ?>
                        <ul id="sidebar">
                            <?php dynamic_sidebar( 'footer_sidebar' ); ?>
                        </ul>
                        <?php endif; ?>
                    </div>
                    <div class="footer-menu col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <?php if ( is_active_sidebar( 'footer_sidebar-2' ) ) : ?>
                        <ul id="sidebar">
                            <?php dynamic_sidebar( 'footer_sidebar-2' ); ?>
                        </ul>
                        <?php endif; ?>
                    </div>
                    <div class="footer-menu col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <?php if ( is_active_sidebar( 'footer_sidebar-3' ) ) : ?>
                        <ul id="sidebar">
                            <?php dynamic_sidebar( 'footer_sidebar-3' ); ?>
                        </ul>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copy col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="footer-copy-content col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <h6>&copy; Marketeros Rockstar. 2017 - <?php _e('Creado con', 'marketeros'); ?> <i class="fa fa-heart"></i> <?php _e('por', 'marketeros'); ?> <a href="http://robertochoa.com.ve">Robert Ochoa</a> - <?php _e('Todos los derechos reservados', 'marketeros'); ?></h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>
</html>
