function change_video(id) {
    "use strict";
    jQuery.ajax({
        url : marketerosAjax.ajax_url,
        type : 'post',
        data : {
            id_post : id,
            action : 'marketeros_video'
        },
        beforeSend: function(){
            jQuery('.home-main-videos-content').html('<div id="loader"></div>');
        },
        success : function( response ) {
            jQuery('.home-main-videos-content').html(response);
        }
    });
}
