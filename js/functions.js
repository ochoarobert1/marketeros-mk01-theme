var searchOpen = false,
    optionsOpen = false,
    headerTop = 0,
    fullPost = 0,
    viewportHeight = 0,
    videoContainer = 0;
/* MAIN ARGUMENTS */
jQuery(document).ready(function ($) {
    "use strict";
     jQuery('.gotop').hide();
    jQuery("#sticker").sticky({
        topSpacing: 0
    });
    viewportHeight = $(window).height();
    headerTop = jQuery('.the-header').outerHeight();

    fullPost = jQuery('.single-main-container').height() - 600;

    videoContainer = jQuery('.home-main-videos-content').outerHeight() - 1;
    jQuery('.home-main-videos-content').css('max-height', videoContainer);
    jQuery('.home-main-videos-content').css('min-height', videoContainer);
    jQuery('.home-main-videos-playlist').height(videoContainer);
    jQuery('.home-main-videos-playlist').css('max-height', videoContainer);
    jQuery('.home-main-videos-playlist').niceScroll();

    /*
     * Let's fire off the gravatar function
     * You can remove this if you don't need it
     */
    jQuery("#main-carousel").owlCarousel({
        center: true,
        items: 1,
        nav: true,
        navText: ['<i class="special-arrow special-arrow-prev"></i>', '<i class="special-arrow special-arrow-next"></i>'],
        loop: true,
        autoplay: false,
        autoplayTimeout: 3000,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut'
    });


}); /* end of as page load scripts */

/* SCROLLING FUNCTIONS */
var scroll = new SmoothScroll('a[href*="#"]', {
    // Selectors
    ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
    header: null, // Selector for fixed headers (must be a valid CSS selector)

    // Speed & Easing
    speed: 500, // Integer. How fast to complete the scroll in milliseconds
    offset: 0, // Integer or Function returning an integer. How far to offset the scrolling anchor location in pixels
    easing: 'easeInOutCubic', // Easing pattern to use
    customEasing: function (time) {}, // Function. Custom easing pattern

    // Callback API
    before: function () {}, // Callback to run before scroll
    after: function () {} // Callback to run after scroll
});
jQuery(document).scroll(function ($) {
    "use strict";

    if (jQuery(document).scrollTop() < videoContainer) {
        jQuery('.gotop').fadeOut();
    } else {
        jQuery('.gotop').fadeIn();
    }

    if (jQuery(document).scrollTop() < headerTop) {
        jQuery('#sticker').unstick();
    } else {
        if (jQuery('#sticker').hasClass('is-sticky')) {
            jQuery('#sticker').sticky('update');
        } else {
            jQuery("#sticker").sticky({
                topSpacing: 0
            });
        }
    }

    jQuery('.home-main-video-item-small-container').niceScroll();

    if (jQuery(document).scrollTop() > fullPost) {
        jQuery('.single-options-container').removeClass('single-options-container-visible');
        jQuery('.single-options-container').addClass('single-options-container-hidden');
    } else {
        jQuery('.single-options-container').addClass('single-options-container-visible');
        jQuery('.single-options-container').removeClass('single-options-container-hidden');
    }
    var s = jQuery(window).scrollTop(),
        d = jQuery(document).height(),
        c = jQuery(window).height(),
        scrollPercent = (s / (d - c)) * 100,
        position = scrollPercent;

    jQuery("#progressbar").attr('value', position);

    jQuery("#progressbar").css('width', '%scrollPercent%');
});

/* CUSTOM FUNCTIONS */
jQuery('#btn-search').on('click touchstart', function () {
    "use strict";
    if (searchOpen === false) {
        jQuery('#btn-search').removeClass('fa-search');
        jQuery('#btn-search').addClass('fa-close');
        jQuery('.navbar-search-container').removeClass('navbar-search-container-hidden');
        jQuery('.navbar-search-container').addClass('navbar-search-container-shown');
        searchOpen = true;
    } else {
        jQuery('#btn-search').addClass('fa-search');
        jQuery('#btn-search').removeClass('fa-close');
        jQuery('.navbar-search-container').removeClass('navbar-search-container-shown');
        jQuery('.navbar-search-container').addClass('navbar-search-container-hidden');
        searchOpen = false;
    }
});

jQuery('.circle-black').click(function () {
    "use strict";
    jQuery('.the-main-body').addClass('the-main-body-black');
});

jQuery('.circle-white').click(function () {
    "use strict";
    jQuery('.the-main-body').removeClass('the-main-body-black');
});

jQuery('#text-large').click(function () {
    "use strict";
    jQuery('.the-single').addClass('the-single-bigger');
});

jQuery('#text-short').click(function () {
    "use strict";
    jQuery('.the-single').removeClass('the-single-bigger');
});

jQuery('.navbar-options-button').click(function () {
    "use strict";
    if (optionsOpen === false) {
        jQuery('.navbar-options-container').removeClass('navbar-options-container-hidden');
        optionsOpen = true;
    } else {
        jQuery('.navbar-options-container').addClass('navbar-options-container-hidden');
        optionsOpen = false;
    }
});
