<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <?php /* MAIN STUFF */ ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
        <meta name="robots" content="NOODP, INDEX, FOLLOW" />
        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="dns-prefetch" href="//connect.facebook.net" />
        <link rel="dns-prefetch" href="//facebook.com" />
        <link rel="dns-prefetch" href="//googleads.g.doubleclick.net" />
        <link rel="dns-prefetch" href="//pagead2.googlesyndication.com" />
        <link rel="dns-prefetch" href="//google-analytics.com" />
        <?php /* FAVICONS */ ?>
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
        <?php /* THEME NAVBAR COLOR */ ?>
        <meta name="msapplication-TileColor" content="#31989F" />
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />
        <meta name="theme-color" content="#31989F" />
        <?php /* AUTHOR INFORMATION */ ?>
        <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
        <meta name="author" content="Marketeros Rockstar" />
        <meta name="copyright" content="http://marketerosrockstar.com" />
        <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
        <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
        <?php wp_title('|', false, 'right'); ?>
        <?php wp_head() ?>
        <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
        <?php get_template_part('includes/header-oginfo'); ?>
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php /* IE COMPATIBILITIES */ ?>
        <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
        <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
        <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
        <!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js" /><!--<![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
        <?php get_template_part('includes/fb-script'); ?>
        <?php get_template_part('includes/ga-script'); ?>
    </head>
    <body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
        <div id="fb-root"></div>

        <header id="top" class="container-fluid" role="banner" itemscope itemtype="http://schema.org/WPHeader">
            <div class="row">
                <div class="the-header col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="the-logo col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">

                        <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs"></div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <a href="<?php echo home_url('/'); ?>" title="<?php _e('Volver al Inicio', 'marketeros'); ?>">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="<?php echo get_bloginfo('name'); ?>" class="img-responsive img-logo" />
                            </a>
                        </div>
                        <div class="header-social-container col-lg-4 col-md-4 col-sm-4 col-xs-12 no-paddingl no-paddingr">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <?php $link = get_option('marketeros_fb'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo $link; ?>" title="<?php _e('Visita nuestra Página de Facebook', 'marketeros'); ?>"><i class="fa fa-facebook"></i></a>
                                <?php } ?>
                                <?php $link = get_option('marketeros_tw'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo $link; ?>" title="<?php _e('Visita nuestro Perfil de Twitter', 'marketeros'); ?>"><i class="fa fa-twitter"></i></a>
                                <?php } ?>
                                <?php $link = get_option('marketeros_yt'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo $link; ?>" title="<?php _e('Visita nuestro Canal de Youtube', 'marketeros'); ?>"><i class="fa fa-youtube"></i></a>
                                <?php } ?>
                                <?php $link = get_option('marketeros_ig'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo $link; ?>" title="<?php _e('Visita nuestro Perfil de Instagram', 'marketeros'); ?>"><i class="fa fa-instagram"></i></a>
                                <?php } ?>
                                <a href="<?php echo home_url('/quienes-somos'); ?>" title="<?php _e('Quienes Somos', 'marketeros'); ?>" class="link_about"><?php _e('Quienes Somos', 'marketeros'); ?></a>
                            </div>
                        </div>

                    </div>
                    <div id="sticker" class="the-navbar col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <div class="the-navbar-desktop col-lg-12 col-md-12 col-sm-12 hidden-xs no-paddingl no-paddingr">
                            <nav class="navbar navbar-default" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                                <div class="container-fluid">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>

                                        <?php if (is_single()) : ?>
                                        <div class="navbar-options-button">
                                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                        </div>

                                        <div class="navbar-options-container navbar-options-container-hidden">
                                            <h3><?php _e('Tamaño:', 'marketeros'); ?></h3>
                                            <div class="navbar-options-content">
                                                <span id="text-short">A</span><span id="text-large">A</span>
                                            </div>
                                            <h3><?php _e('Color:', 'marketeros'); ?></h3>
                                            <div class="navbar-options-content">
                                                <span class="circle circle-white"></span><span class="circle circle-black"></span>
                                            </div>
                                        </div>

                                        <?php endif; ?>

                                        <div class="navbar-search-container navbar-search-container-shown">
                                            <?php get_search_form(); ?>
                                        </div>
                                    </div>

                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <?php $theme_locations = get_nav_menu_locations(); ?>
                                    <?php $menu_obj = get_term( $theme_locations['header_menu'] ); ?>
                                    <?php echo special_menu_container($menu_obj->name); ?>
                                </div><!-- /.container-fluid -->
                            </nav>
                        </div>
                        <div class="the-navbar-mobile hidden-lg hidden-md hidden-sm col-xs-12 no-paddingl no-paddingr">
                            <nav class="navbar navbar-default" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                                <div class="container-fluid">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                    <?php     wp_nav_menu( array(
    'theme_location'    => 'header_menu',
    'depth'             => 2,
    'container'         => 'div',
    'container_class'   => 'collapse navbar-collapse',
    'container_id'      => 'bs-example-navbar-collapse-2',
    'menu_class'        => 'nav navbar-nav',
    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
    'walker'            => new wp_bootstrap_navwalker())
                                                         ); ?>

                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
