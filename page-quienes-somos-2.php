<?php get_header(); ?>
<?php the_post(); ?>
<main class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <h1 itemprop="headline"><?php the_title(); ?></h1>
            <div class="the-breadcrumbs col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo the_breadcrumb(); ?>
            </div>
            <article id="post-<?php the_ID(); ?>" class="page-content <?php echo join(' ', get_post_class()); ?>" >
                <div class="page-article col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" itemprop="articleBody">
                    <section class="about-us col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <div class="container">
                            <div class="row">
                                <div class="about-us-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h2><?php _e('NUESTRO EQUIPO', 'marketeros'); ?></h2>
                                    <?php $args = array('post_type' => 'miembros', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date');  ?>
                                    <?php query_posts($args); ?>
                                    <?php if (have_posts()) : ?>
                                    <div class="col-md-10 col-md-offset-1">
                                        <?php while (have_posts()) : the_post(); ?>
                                        <div class="miembro-item col-md-6">
                                            <picture>
                                                <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                                            </picture>
                                            <h3><?php the_title(); ?></h3>
                                            <p><?php the_content(); ?></p>
                                        </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <?php endif; ?>
                                    <?php wp_reset_query(); ?>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </article>
        </section>
    </div>
</main>
<?php get_footer(); ?>

