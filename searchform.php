<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
    <div class="input-group">
        <label for="s" class="screen-reader-text"><?php _e('Buscar por Términos:','marketeros'); ?></label>
        <input  type="search" id="s" name="s" class="form-control" placeholder="<?php _e('Buscar:','marketeros'); ?>">
        <span class="input-group-btn">
            <button class="btn btn-default"  type="submit" id="searchsubmit"><i class="fa fa-search"></i></button>
        </span>
    </div><!-- /input-group -->
</form>
