<?php get_header(); ?>
<?php the_post(); ?>

<main class="container">
    <div class="row">
        <div class="single-main-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
            <?php $defaultargs = array('class' => 'img-responsive'); ?>
            <?php /* GET THE POST FORMAT */ ?>
            <?php get_template_part( 'post-formats/format', get_post_format() ); ?>
            <aside class="the-sidebar col-lg-4 col-md-4 col-sm-4 hidden-xs" role="complementary">
                <?php get_sidebar('single'); ?>
            </aside>
        </div>
    </div>
</main>
<div class="single-options-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
    <div class="single-options-content col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
        <div class="custom-progress">
            <progress id="progressbar" value="0" max="100"></progress>
        </div>
        <div class="single-options-title-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><?php the_title(); ?></h3>
        </div>
    </div>
</div>
<?php get_footer(); ?>
