<?php if ( is_active_sidebar( 'single_sidebar' ) ) : ?>
<ul id="sidebar">
    <?php dynamic_sidebar( 'single_sidebar' ); ?>
</ul>
<?php endif; ?>
