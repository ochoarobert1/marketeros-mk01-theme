<h3 class="comment-main-title"><?php _e('Deja tu comentario', 'marketeros'); ?></h3>
    <div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#facebook" aria-controls="facebook" role="tab" data-toggle="tab"><i class="fa fa-facebook-official"></i> Facebook</a></li>
        <li role="presentation"><a href="#wordpress" aria-controls="wordpress" role="tab" data-toggle="tab"><i class="fa fa-wordpress"></i> WordPress</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="facebook">
            <div class="single-comments col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="5"></div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="wordpress">
            <div class="single-comments single-comments-wordpress col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                <?php comment_form(); ?>
                <h5><?php _e('Comentarios Recientes:', 'marketeros') ?></h5>
                <?php wp_list_comments(); ?>
                <?php paginate_comments_links(); ?>
            </div>

        </div>

    </div>

</div>
