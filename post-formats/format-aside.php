
<?php /* POST FORMAT - DEFAULT */ ?>

<article id="post-<?php the_ID(); ?>" class="the-single col-lg-8 col-md-8 col-sm-8 col-xs-12 <?php echo join(' ', get_post_class()); ?>" itemscope itemtype="http://schema.org/Article">

    <header>
        <div class="post-categories"><?php the_category(' '); ?></div>
        <?php $category = get_the_category(); ?>
        <?php $subtitulo = get_post_meta(get_the_ID(), 'rw_post_subtitle', true); ?>
        <?php if ($subtitulo != '') { ?>
        <h3 class="single-subtitle"><?php echo $subtitulo; ?></h3>
        <?php } ?>
        <h1 class="single-title" itemprop="name"><?php the_title(); ?></h1>
        <span class="date"><?php the_time('M j, Y'); ?></span>
    </header>
    <div class="post-content" itemprop="articleBody">
        <?php if ( has_post_thumbnail()) : ?>
        <picture>
            <?php the_post_thumbnail('single_img', array('class' => 'img-responsive img-featured-single')); ?>
        </picture>
        <?php endif; ?>
        <?php the_content() ?>
        <?php wp_link_pages( array(
    'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'marketeros' ) . '</span>',
    'after'       => '</div>',
    'link_before' => '<span>',
    'link_after'  => '</span>', ) ); ?>
        <footer>

            <div class="related-posts-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                <h2><?php _e('Te puede interesar', 'marketeros'); ?></h2>
                <?php $args = array('post_type' => 'post', 'posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date', 'ignore_sticky_posts' => 1, 'post__not_in' => array(get_the_ID()), 'category_name' => $category[0]->slug); ?>
                <?php query_posts($args); ?>
                <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                <div class="related-item col-lg-4 col-md-4 col-sm-4 col-xs-12 no-paddingl">
                    <a href="<?php the_permalink();?>" title="<?php echo get_the_title(); ?>">
                        <?php the_post_thumbnail('blog_img', array('class' => 'img-responsive', 'itemprop' => 'image')); ?>
                    </a>
                    <a href="<?php the_permalink();?>" title="<?php echo get_the_title(); ?>">
                        <h3><?php the_title(); ?></h3>
                    </a>
                </div>
                <?php endwhile; ?>
                <?php endif; wp_reset_query(); ?>
            </div>
            <?php the_tags( __( 'Tags: ', 'marketeros' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>
        </footer>
    </div><!-- .post-content -->
    <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
    <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
    <meta itemprop="url" content="<?php the_permalink() ?>">
    <?php if ( comments_open() ) { comments_template(); } ?>
</article> <?php // end article ?>
