<?php

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

require_once('includes/wp_enqueue_styles.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", '');
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '3.0.0', false);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', false, '1.12.4', false);
    }
    wp_enqueue_script('jquery');
}

/* NOW ALL THE JS FILES */
require_once('includes/wp_enqueue_scripts.php');

/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

// WALKER COMPLETO TOMADO DESDE EL NAVBAR COLLAPSE
require_once('includes/wp_bootstrap_navwalker.php');

// WALKER CUSTOM SI DEBO COLOCAR ICONOS AL LADO DEL MENU PRINCIPAL - SU ESTRUCTURA ESTA DENTRO DEL MISMO ARCHIVO
//require_once('includes/wp_walker_custom.php');

/* --------------------------------------------------------------
    ADD MENU WALKER
-------------------------------------------------------------- */

function wp_get_menu_array($current_menu) {

    $array_menu = wp_get_nav_menu_items($current_menu);
    $menu = array();
    foreach ($array_menu as $m) {
        if (empty($m->menu_item_parent)) {
            $menu[$m->ID] = array();
            $menu[$m->ID]['ID']      =   $m->ID;
            $menu[$m->ID]['title']       =   $m->title;
            $menu[$m->ID]['url']         =   $m->url;
            $menu[$m->ID]['children']    =   array();
        }
    }
    $submenu = array();
    foreach ($array_menu as $m) {
        if ($m->menu_item_parent) {
            $submenu[$m->ID] = array();
            $submenu[$m->ID]['ID']       =   $m->ID;
            $submenu[$m->ID]['title']    =   $m->title;
            $submenu[$m->ID]['url']  =   $m->url;
            $menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
        }
    }
    return $menu;

}
// WALKER CUSTOM PARA BOTONES
require_once('includes/wp_special_menu.php');

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

require_once('includes/wp_bootstrap_gallery.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */

//require_once('includes/wp_woocommerce_functions.php');

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */

load_theme_textdomain( 'marketeros', get_template_directory() . '/languages' );
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio' ));
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );
add_theme_support( 'menus' );
add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form' ) );
add_theme_support( 'custom-background',
                  array(
                      'default-image' => '',    // background image default
                      'default-color' => '',    // background color default (dont add the #)
                      'wp-head-callback' => '_custom_background_cb',
                      'admin-head-callback' => '',
                      'admin-preview-callback' => ''
                  )
                 );

/* --------------------------------------------------------------
    ADD CUSTOM EDITOR STYLE
-------------------------------------------------------------- */
function marketeros_add_editor_styles() {
    add_editor_style( get_stylesheet_directory_uri() . '/css/editor-styles.css' );
}
add_action( 'admin_init', 'marketeros_add_editor_styles' );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus( array(
    'header_menu' => __( 'Menu Header - Principal', 'marketeros' ),
    'footer_menu' => __( 'Menu Footer - Principal', 'marketeros' ),
) );

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action( 'widgets_init', 'marketeros_widgets_init' );
function marketeros_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Sidebar Principal', 'marketeros' ),
        'id' => 'main_sidebar',
        'description' => __( 'Estos widgets seran vistos en posts y pages', 'marketeros' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ) );

    register_sidebar( array(
        'name' => __( 'Sidebar en Entradas', 'marketeros' ),
        'id' => 'single_sidebar',
        'description' => __( 'Estos widgets seran vistos en Tienda y Categorias de Producto', 'marketeros' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ) );
    register_sidebars( 3, array(
        'name' => __( 'Home Widget Nro. %d', 'marketeros' ),
        'id'            => 'home_sidebar',
        'description'   => '',
        'class'         => '',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ) );
    register_sidebars( 3, array(
        'name' => __( 'Footer Widget Nro. %d', 'marketeros' ),
        'id'            => 'footer_sidebar',
        'description'   => '',
        'class'         => '',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ) );

}

/* --------------------------------------------------------------
    CUSTOM ADMIN LOGIN
-------------------------------------------------------------- */

function custom_login_logo() {
    $version_remove = NULL;
    wp_register_style('wp-custom-login', get_template_directory_uri() . '/css/custom-wordpress-admin-style.css', false, $version_remove, 'all');
    wp_enqueue_style('wp-custom-login');

}
add_action('login_head', 'custom_login_logo');
add_action('admin_head', 'custom_login_logo');

if (! function_exists('dashboard_footer') ){
    function dashboard_footer() {
        echo '<span id="footer-thankyou">';
        _e ('Gracias por crear con ', 'marketeros' );
        echo '<a href="http://wordpress.org/" target="_blank">WordPress.</a> - ';
        _e ('Tema desarrollado por ', 'marketeros' );
        echo '<a href="http://robertochoa.com.ve/?utm_source=footer_admin&utm_medium=link&utm_content=marketeros" target="_blank">Robert Ochoa</a></span>';
    }
}
add_filter('admin_footer_text', 'dashboard_footer');

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

require_once('includes/wp_custom_metabox.php');

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

require_once('includes/wp_custom_post_type.php');

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if ( function_exists('add_theme_support') ) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size( 9999, 400, true);
}
if ( function_exists('add_image_size') ) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('mini_post', 150, 150, array('center', 'center'));
    add_image_size('slider_img', 900, 450, array('center', 'center'));
    add_image_size('blog_img', 360, 250, true);
    add_image_size('single_img', 636, 297, true );
}

/* --------------------------------------------------------------
    ADD CUSTOM THEME CONTROLS
-------------------------------------------------------------- */

require_once('includes/wp_custom_theme_control.php');

function sum_video_parser($url, $autoplay, $wdth=320, $hth=320){
    $iframe = '';
    $thumb_link = '';
    if (isset($autoplay)){
        if ($autoplay == true){
            $auto = '?autoplay=1';
        } else {
            $auto = '?autoplay=0';
        }
    }
    if (is_array($url)) {
        $url = $url[0];
    }
    if (strpos($url, 'youtube.com') !== FALSE) {
        $step1=explode('v=', $url);
        $step2 =explode('&amp;',$step1[1]);
        if (strlen($step2[0]) > 12){
            $step3 = substr($step2[0], 0, 11);
        } else {
            $step3 = $step2[0];
        }
        $iframe ='<iframe width="'.$wdth.'" height="'.$hth.'" src="http://www.youtube.com/embed/'.$step2[0].$auto.'&showinfo=0&modestbranding=1&rel=0" frameborder="0"></iframe>';
        $embed ='<object width="'.$wdth.'" height="'.$hth.'" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"><param name="src" value="http://www.youtube.com/v/'.$step2[0].'&'.$auto.'" /><param name="wmode" value="transparent" /><param name="embed" value="" /><embed width="'.$wdth.'" height="'.$hth.'" type="application/x-shockwave-flash" src="http://www.youtube.com/v/'.$step2[0].$auto.'" wmode="transparent" embed="" /></object>';
        $thumbnail_str = 'http://img.youtube.com/vi/'.$step3.'/2.jpg';
        $fullsize_str = 'http://img.youtube.com/vi/'.$step3.'/0.jpg';
        $thumb_link = htmlentities($fullsize_str);

    }
    else if (strpos($url, 'vimeo') !== FALSE) {

        $id=str_replace('http://vimeo.com/','',$url);
        $embedurl = "http://player.vimeo.com/video/".$id;

        $iframe = '<iframe  src="'.$embedurl.'"
 width="'.$wdth.'" height="'.$hth.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen>
</iframe>';
        $embed ='
<embed width="'.$wdth.'" height="'.$hth.'" type="application/x-shockwave-flash"
src="'.$embedurl.'"  />
';
        $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
        if (!empty($hash) && is_array($hash)) {
            //$video_str = 'http://vimeo.com/moogaloop.swf?clip_id=%s';
            $thumbnail_str = $hash[0]['thumbnail_small'];
            $fullsize_str = '<img src="'.$hash[0]['thumbnail_large'].'" />';

        }

    }


    return array("embed"=>$iframe,"thumb_image" =>$thumb_link);

}

function get_youtube_id ($url) {
    $step1=explode('v=', $url);
    $step2 =explode('&amp;',$step1[1]);
    if (strlen($step2[0]) > 12){
        $step3 = substr($step2[0], 0, 11);
    } else {
        $step3 = $step2[0];
    }
    return $step3;
}

/* --------------------------------------------------------------
    ADD AJAX HELPER
-------------------------------------------------------------- */

add_action( 'wp_enqueue_scripts', 'ajax_marketeros_enqueue_scripts' );
function ajax_marketeros_enqueue_scripts() {
    wp_enqueue_script( 'marketerosAjax', get_template_directory_uri() . '/js/ajax-scripts.js', array('jquery'), '1.0', true );

    wp_localize_script( 'marketerosAjax', 'marketerosAjax', array(
        'ajax_url' => admin_url( 'admin-ajax.php' )
    ));
}

add_action('wp_ajax_nopriv_marketeros_video','marketeros_video');
add_action('wp_ajax_marketeros_video','marketeros_video');

function marketeros_video()
{

    $id_post = $_POST['id_post'];



    $video = get_post_meta($id_post, 'rw_post_video', true);
    $embed = sum_video_parser($video, 1); ?>
<div class="embed-responsive embed-responsive-16by9">
    <?php echo $embed['embed']; ?>
</div>
<?php
    wp_die();
}

?>
