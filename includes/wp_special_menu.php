<?php
function special_menu_container($menu_special) {
?>
<ul class="nav navbar-nav navbar-custom">

    <?php $arrayitems = wp_get_menu_array($menu_special); ?>
    <?php $i = 1; ?>

    <?php foreach ($arrayitems as $items) {?>
    <?php $array_posts = array(); ?>
    <li class="navbar-custom-li nav_main_<?php echo $i; ?>" id="<?php echo $i; ?>">
        <?php $item_term = $items['url']; ?>
        <?php $variable = explode('/secciones/', $item_term); ?>

        <?php $item_var = $variable[1]; ?>
        <?php $term_name = strstr($item_var, '/', true); ?>
        <?php $term = get_term_by('slug', $term_name, 'category');  ?>

        <a class="navbar-custom-link" href="<?php echo $items['url']; ?>">
            <span><?php echo $items['title']; ?></span>
            <div>
                <?php $image_id = get_term_meta ( $term->term_id, 'custom_servicio_image', true ); ?>
                <?php echo wp_get_attachment_image ( $image_id, 'full', "", array( "class" => "img-responsive img-cat-logo" ) ); ?>
            </div>
        </a>
    </li>
    <?php $i++; } ?>
</ul>
<?php }
