<?php
/* --------------------------------------------------------------
CUSTOM AREA FOR OPTIONS DATA - marketeros
-------------------------------------------------------------- */

/* CUSTOM MENU PAGE AND FUNCTIONS IN ADMIN */
function register_marketeros_settings() {
    //register our settings
    register_setting( 'marketeros-settings-group', 'marketeros_dir' );
    register_setting( 'marketeros-settings-group', 'marketeros_email' );
    register_setting( 'marketeros-settings-group', 'marketeros_telf' );
    register_setting( 'marketeros-settings-group', 'marketeros_mob' );
    register_setting( 'marketeros-settings-group', 'marketeros_fb' );
    register_setting( 'marketeros-settings-group', 'marketeros_tw' );
    register_setting( 'marketeros-settings-group', 'marketeros_ig' );
    register_setting( 'marketeros-settings-group', 'marketeros_yt' );
}

function my_admin_menu() {
    add_menu_page( 'Opciones del Sitio', 'Opciones del Sitio', 'manage_options', 'marketeros_custom_options', 'my_custom_menu_page', get_template_directory_uri() . '/images/plugin-icon.png', 120  );
    /* call register settings function */
    add_action( 'admin_init', 'register_marketeros_settings' );
}

add_action( 'admin_menu', 'my_admin_menu' );



/* CUSTOM CSS FOR THIS SECTION */
function load_custom_wp_admin_style($hook) {
    if( $hook != 'toplevel_page_marketeros_custom_options' ) {
        return;
    }
    /* ENQUEUE THE CSS */
    wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&amp;subset=latin-ext');
    wp_enqueue_style( 'custom_wp_admin_css', get_template_directory_uri() . '/css/custom-wordpress-admin-style.css' );
}

add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

/* SEPARATOR FOR STYLING THE CUSTOM PAGE */
function add_admin_menu_separator( $position ) {

    global $menu;

    $menu[ $position ] = array(
        0    =>    '',
        1    =>    'read',
        2    =>    'separator' . $position,
        3    =>    '',
        4    =>    'wp-menu-separator'
    );

}

function set_admin_menu_separator() {  do_action( 'admin_init', 119 );  }

add_action( 'admin_init', 'add_admin_menu_separator' );
add_action( 'admin_menu', 'set_admin_menu_separator' );

/* CUSTOM MENU PAGE CONTENT */
function my_custom_menu_page() { ?>

<div class="marketeros_custom_options-header">
    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="<?php echo get_bloginfo('name'); ?>" class="logo-header" />
    <h1><?php echo get_admin_page_title(); ?></h1>
    <div class="custom-clearfix"></div>
</div>
<div class="marketeros_custom_options-content">
    <form method="post" action="options.php">
        <?php settings_fields( 'marketeros-settings-group' ); ?>
        <?php do_settings_sections( 'marketeros-settings-group' ); ?>
        <table class="form-table">

            <tr valign="top">
                <th scope="row"><?php _e('Dirección', 'marketeros'); ?></th>
                <td><textarea name="marketeros_dir" cols="95" rows="5"><?php echo esc_attr( get_option('marketeros_dir') ); ?></textarea></td>
            </tr>

            <tr valign="top">
                <th scope="row"><?php _e('Correo Electrónico', 'marketeros'); ?></th>
                <td><input type="text" size="90" name="marketeros_email" value="<?php echo esc_attr( get_option('marketeros_email') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row"><?php _e('Teléfono', 'marketeros'); ?></th>
                <td><input type="text" size="90" name="marketeros_telf" value="<?php echo esc_attr( get_option('marketeros_telf') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row"><?php _e('Móvil', 'marketeros'); ?></th>
                <td><input type="text" size="90" name="marketeros_mob" value="<?php echo esc_attr( get_option('marketeros_mob') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row" colspan="2"><h3><?php _e('Redes Sociales', 'marketeros'); ?></h3></th>
            </tr>

            <tr valign="top">
                <th scope="row"><?php _e('Perfil de Facebook', 'marketeros'); ?></th>
                <td><input type="text" size="90" name="marketeros_fb" value="<?php echo esc_attr( get_option('marketeros_fb') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row"><?php _e('Perfil de Twitter', 'marketeros'); ?></th>
                <td><input type="text" size="90" name="marketeros_tw" value="<?php echo esc_attr( get_option('marketeros_tw') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row"><?php _e('Perfil de Instagram', 'marketeros'); ?></th>
                <td><input type="text" size="90" name="marketeros_ig" value="<?php echo esc_attr( get_option('marketeros_ig') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row"><?php _e('Canal de Youtube', 'marketeros'); ?></th>
                <td><input type="text" size="90" name="marketeros_yt" value="<?php echo esc_attr( get_option('marketeros_yt') ); ?>" /></td>
            </tr>

        </table>
        <?php submit_button(); ?>
    </form>
</div>
<?php }




