<?php
/* --------------------------------------------------------------
    CREATE CUSTOM METABOXES
-------------------------------------------------------------- */

add_filter( 'rwmb_meta_boxes', 'marketeros_metabox' );


function marketeros_metabox( $meta_boxes )
{
    $prefix = 'rw_';

    $meta_boxes[] = array(
        'id'         => 'post_data',
        'title'      => __( 'Información Extra - Entrada', 'marketeros' ),
        'post_types' => array( 'post' ),
        'context'    => 'side',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'     => __( 'Subtitulo del Post', 'marketeros' ),
                'id'       => $prefix . 'post_subtitle',
                'desc'     => __( 'Descripción breve que va antes del titulo', 'marketeros' ),
                'type'     => 'text'
            ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'video_data',
        'title'      => __( 'Información Extra - Videos', 'marketeros' ),
        'post_types' => array( 'videos' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'     => __( 'Link del Video', 'marketeros' ),
                'id'       => $prefix . 'post_video',
                'desc'     => __( 'Link de Youtube para entrada tipo Video', 'marketeros' ),
                'type'     => 'text',
                'size'     => 90
            ),
        )
    );

    return $meta_boxes;
}

