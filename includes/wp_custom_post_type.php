<?php

// Register Custom Post Type
function miembros() {

    $labels = array(
        'name'                  => _x( 'Miembros', 'Post Type General Name', 'marketeros' ),
        'singular_name'         => _x( 'Miembro', 'Post Type Singular Name', 'marketeros' ),
        'menu_name'             => __( 'Miembros', 'marketeros' ),
        'name_admin_bar'        => __( 'Miembros', 'marketeros' ),
        'archives'              => __( 'Archivo de Miembros', 'marketeros' ),
        'attributes'            => __( 'Atributos del Miembro', 'marketeros' ),
        'parent_item_colon'     => __( 'Miembro Padre:', 'marketeros' ),
        'all_items'             => __( 'Todos los Miembros', 'marketeros' ),
        'add_new_item'          => __( 'Agregar Nuevo Miembro', 'marketeros' ),
        'add_new'               => __( 'Agregar Nuevo', 'marketeros' ),
        'new_item'              => __( 'Nuevo Miembro', 'marketeros' ),
        'edit_item'             => __( 'Editar Miembro', 'marketeros' ),
        'update_item'           => __( 'Actualizar Miembro', 'marketeros' ),
        'view_item'             => __( 'Ver Miembro', 'marketeros' ),
        'view_items'            => __( 'Ver Miembros', 'marketeros' ),
        'search_items'          => __( 'Buscar Miembro', 'marketeros' ),
        'not_found'             => __( 'No hay resultados', 'marketeros' ),
        'not_found_in_trash'    => __( 'No hay resultados en papelera', 'marketeros' ),
        'featured_image'        => __( 'Foto Destacada', 'marketeros' ),
        'set_featured_image'    => __( 'Colocar Foto Destacada', 'marketeros' ),
        'remove_featured_image' => __( 'Remover Foto Destacada', 'marketeros' ),
        'use_featured_image'    => __( 'Usar como Foto Destacada', 'marketeros' ),
        'insert_into_item'      => __( 'Insertar en Miembro', 'marketeros' ),
        'uploaded_to_this_item' => __( 'Cargado a este Miembro', 'marketeros' ),
        'items_list'            => __( 'Listado de Miembros', 'marketeros' ),
        'items_list_navigation' => __( 'Navegación del Listado de Miembros', 'marketeros' ),
        'filter_items_list'     => __( 'Filtro del Listado de Miembros', 'marketeros' ),
    );
    $args = array(
        'label'                 => __( 'Miembro', 'marketeros' ),
        'description'           => __( 'Miembros dentro de la empresa', 'marketeros' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'dashicons-groups',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => false,
    );
    register_post_type( 'miembros', $args );

}
add_action( 'init', 'miembros', 0 );

// Register Custom Post Type
function videos() {

    $labels = array(
        'name'                  => _x( 'Videos', 'Post Type General Name', 'marketeros' ),
        'singular_name'         => _x( 'Video', 'Post Type Singular Name', 'marketeros' ),
        'menu_name'             => __( 'Videos', 'marketeros' ),
        'name_admin_bar'        => __( 'Videos', 'marketeros' ),
        'archives'              => __( 'Archivo de Videos', 'marketeros' ),
        'attributes'            => __( 'Atributos de Video', 'marketeros' ),
        'parent_item_colon'     => __( 'Video Padre:', 'marketeros' ),
        'all_items'             => __( 'Todos los Videos', 'marketeros' ),
        'add_new_item'          => __( 'Agregar Nuevo Video', 'marketeros' ),
        'add_new'               => __( 'Agregar Nuevo', 'marketeros' ),
        'new_item'              => __( 'Nuevo Video', 'marketeros' ),
        'edit_item'             => __( 'Editar Video', 'marketeros' ),
        'update_item'           => __( 'Actualizar Video', 'marketeros' ),
        'view_item'             => __( 'Ver Videos', 'marketeros' ),
        'view_items'            => __( 'Ver Videos', 'marketeros' ),
        'search_items'          => __( 'Buscar Video', 'marketeros' ),
        'not_found'             => __( 'No hay resultados', 'marketeros' ),
        'not_found_in_trash'    => __( 'No hay resultados en la papelera', 'marketeros' ),
        'featured_image'        => __( 'Imagen Destacada', 'marketeros' ),
        'set_featured_image'    => __( 'Colocar Imagen Destacada', 'marketeros' ),
        'remove_featured_image' => __( 'Remover Imagen Destacada', 'marketeros' ),
        'use_featured_image'    => __( 'Usar como Imagen Destacada', 'marketeros' ),
        'insert_into_item'      => __( 'Insertar en Video', 'marketeros' ),
        'uploaded_to_this_item' => __( 'Cargado a este Video', 'marketeros' ),
        'items_list'            => __( 'Listado de Videos', 'marketeros' ),
        'items_list_navigation' => __( 'Navegación del Listado de Videos', 'marketeros' ),
        'filter_items_list'     => __( 'Filtro del Listado de Videos', 'marketeros' ),
    );
    $args = array(
        'label'                 => __( 'Video', 'marketeros' ),
        'description'           => __( 'Videos Destacados', 'marketeros' ),
        'labels'                => $labels,
        'supports'              => array( 'title' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-playlist-video',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'videos', $args );

}
add_action( 'init', 'videos', 0 );

if( ! class_exists( 'Showcase_Taxonomy_Images' ) ) {
    class Showcase_Taxonomy_Images {

        public function __construct() {
            //
        }

        public function init() {
            // Image actions
            add_action( 'category_add_form_fields', array( $this, 'add_category_image' ), 10, 2 );
            add_action( 'created_category', array( $this, 'save_category_image' ), 10, 2 );
            add_action( 'category_edit_form_fields', array( $this, 'update_category_image' ), 10, 2 );
            add_action( 'edited_category', array( $this, 'updated_category_image' ), 10, 2 );
            add_action( 'admin_enqueue_scripts', array( $this, 'load_media' ) );
            add_action( 'admin_footer', array( $this, 'add_script' ) );
        }

        public function load_media() {
            if( ! isset( $_GET['taxonomy'] ) || $_GET['taxonomy'] != 'category' ) {
                return;
            }
            wp_enqueue_media();
        }


        public function add_category_image( $taxonomy ) { ?>
<div class="form-field term-group">
    <label for="custom_servicio_image"><?php _e( 'Ícono', 'boterosoto' ); ?></label>
    <input type="hidden" id="custom_servicio_image" name="custom_servicio_image" class="custom_media_url" value="">
    <div id="category-image-wrapper"></div>
    <p>
        <input type="button" class="button button-secondary showcase_tax_media_button" id="showcase_tax_media_button" name="showcase_tax_media_button" value="<?php _e( 'Agregar Ícono', 'boterosoto' ); ?>" />
        <input type="button" class="button button-secondary showcase_tax_media_remove" id="showcase_tax_media_remove" name="showcase_tax_media_remove" value="<?php _e( 'Remover Ícono', 'boterosoto' ); ?>" />
    </p>
</div>
<?php }

        public function save_category_image( $term_id, $tt_id ) {
            if( isset( $_POST['custom_servicio_image'] ) && '' !== $_POST['custom_servicio_image'] ){
                add_term_meta( $term_id, 'custom_servicio_image', absint( $_POST['custom_servicio_image'] ), true );
            }
        }

        public function update_category_image( $term, $taxonomy ) { ?>
<tr class="form-field term-group-wrap">
    <th scope="row">
        <label for="custom_servicio_image"><?php _e( 'Ícono', 'boterosoto' ); ?></label>
    </th>
    <td>
        <?php $image_id = get_term_meta( $term->term_id, 'custom_servicio_image', true ); ?>
        <input type="hidden" id="custom_servicio_image" name="custom_servicio_image" value="<?php echo esc_attr( $image_id ); ?>">
        <div id="category-image-wrapper">
            <?php if( $image_id ) { ?>
            <?php echo wp_get_attachment_image( $image_id, 'thumbnail' ); ?>
            <?php } ?>
        </div>
        <p>
            <input type="button" class="button button-secondary showcase_tax_media_button" id="showcase_tax_media_button" name="showcase_tax_media_button" value="<?php _e( 'Agregar Ícono', 'boterosoto' ); ?>" />
            <input type="button" class="button button-secondary showcase_tax_media_remove" id="showcase_tax_media_remove" name="showcase_tax_media_remove" value="<?php _e( 'Remover Ícono', 'boterosoto' ); ?>" />
        </p>
    </td>
</tr>
<?php }


        public function updated_category_image( $term_id, $tt_id ) {
            if( isset( $_POST['custom_servicio_image'] ) && '' !== $_POST['custom_servicio_image'] ){
                update_term_meta( $term_id, 'custom_servicio_image', absint( $_POST['custom_servicio_image'] ) );
            } else {
                update_term_meta( $term_id, 'custom_servicio_image', '' );
            }
        }


        public function add_script() {
            if( ! isset( $_GET['taxonomy'] ) || $_GET['taxonomy'] != 'category' ) {
                return;
            } ?>
<script> jQuery(document).ready( function($) {
        _wpMediaViewsL10n.insertIntoPost = '<?php _e( "Insert", "showcase" ); ?>';
        function ct_media_upload(button_class) {
            var _custom_media = true, _orig_send_attachment = wp.media.editor.send.attachment;
            $('body').on('click', button_class, function(e) {
                var button_id = '#'+$(this).attr('id');
                var send_attachment_bkp = wp.media.editor.send.attachment;
                var button = $(button_id);
                _custom_media = true;
                wp.media.editor.send.attachment = function(props, attachment){
                    if( _custom_media ) {
                        $('#custom_servicio_image').val(attachment.id);
                        $('#category-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
                        $( '#category-image-wrapper .custom_media_image' ).attr( 'src',attachment.url ).css( 'display','block' );
                    } else {
                        return _orig_send_attachment.apply( button_id, [props, attachment] );
                    }
                }
                wp.media.editor.open(button); return false;
            });
        }
        ct_media_upload('.showcase_tax_media_button.button');
        $('body').on('click','.showcase_tax_media_remove',function(){
            $('#custom_servicio_image').val('');
            $('#category-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
        });
        // Thanks: http://stackoverflow.com/questions/15281995/wordpress-create-category-ajax-response
        $(document).ajaxComplete(function(event, xhr, settings) {
            var queryStringArr = settings.data.split('&');
            if( $.inArray('action=add-tag', queryStringArr) !== -1 ){
                var xml = xhr.responseXML;
                $response = $(xml).find('term_id').text();
                if($response!=""){
                    // Clear the thumb image
                    $('#category-image-wrapper').html('');
                }
            }
        });
    });
</script>
<?php }
    }
    $Showcase_Taxonomy_Images = new Showcase_Taxonomy_Images();
    $Showcase_Taxonomy_Images->init(); }

